from django import forms
from django.contrib.auth.forms import UserCreationForm
from account.models import User


class RegistrationForm(UserCreationForm):
    username = forms.CharField(required=False)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=False)
    email = forms.EmailField(required=True)
    send_mode = forms.BooleanField(required=False)

    class Meta:
        model = User
        fields = ('email', 'username', 'first_name', 'last_name', 'send_mode', 'password1', 'password2')

