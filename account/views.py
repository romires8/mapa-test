import json
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as auth_logout, login
from django.contrib.auth import get_user_model

from social_core.backends.oauth import BaseOAuth1, BaseOAuth2
from social_core.backends.google import GooglePlusAuth
from social_core.backends.utils import load_backends
from social_django.utils import psa, load_strategy

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, Http404, get_list_or_404, redirect

from account.forms import RegistrationForm
from account.models import User

redirect_field_name = 'next'


def registration(request):
    """ User registration """
    if request.user.is_authenticated:
        return redirect('index')

    redirect_to = request.POST.get(redirect_field_name, request.GET.get(redirect_field_name, '/login/'))
    form = RegistrationForm(data=request.POST or None)

    if request.method == 'POST':

        if form.is_valid():
            email = form.cleaned_data.get('email')
            if User.objects.filter(email=email).count() == 0:
                user = User(
                    username=email,
                    email=email,
                    first_name=form.cleaned_data.get('first_name'),
                    last_name=form.cleaned_data.get('last_name'),
                    is_active=True,
                    is_subscribed=form.cleaned_data.get('send_mode'),
                )
                user.set_password(form.cleaned_data.get('password1'))
                user.save()

                # _send_email_confirmation(user)
                if redirect_to:
                    return HttpResponseRedirect(redirect_to)
                return HttpResponseRedirect("/")
            else:
                form.add_error("email", "This email address is already registered")

    return render(request, 'registration/registration_form.html',
                  {'form': form, 'title': "Sign up"})


@psa('social:complete')
def ajax_auth(request, backend):
    """AJAX authentication endpoint"""
    if isinstance(request.backend, BaseOAuth1):
        token = {
            'oauth_token': request.REQUEST.get('access_token'),
            'oauth_token_secret': request.REQUEST.get('access_token_secret'),
        }
    elif isinstance(request.backend, BaseOAuth2):
        token = request.REQUEST.get('access_token')
    else:
        raise HttpResponseBadRequest('Wrong backend type')
    user = request.backend.do_auth(token, ajax=True)
    login(request, user)
    data = {'id': user.id, 'username': user.username}
    return HttpResponse(json.dumps(data), mimetype='application/json')
