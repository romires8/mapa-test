from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import (
    UserManager as BaseUserManager, User as BaseUser, AbstractBaseUser, PermissionsMixin
)
from django.core import validators


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(_('username'), max_length=120, unique=True,
                                help_text=_('Required. 30 characters or fewer. Letters, digits and '
                                            '@/./+/-/_ only.'),
                                validators=[
                                    validators.RegexValidator(r'^[\w.@+-]+$', _('Enter a valid username.'), 'invalid')
                                ])
    email = models.EmailField(verbose_name='E-mail', max_length=255, unique=True)
    first_name = models.CharField(verbose_name='First name', max_length=32, blank=True)
    last_name = models.CharField(verbose_name='Last name', max_length=32)
    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_subscribed = models.BooleanField(verbose_name='Subscribed to receive PlacePass news and offers', default=False)
    is_staff = models.BooleanField(default=False)

    objects = BaseUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'email']

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def __str__(self):
        return self.email
