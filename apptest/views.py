from django.shortcuts import redirect, render


def home(request):
    """ Home view """
    if not request.user.is_authenticated:
        return redirect('registration')
    return render(request, 'index.html')
